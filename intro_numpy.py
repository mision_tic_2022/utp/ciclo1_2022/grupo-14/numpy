import numpy as np
from numpy import random as r

numeros = np.array( [ [10, 20], [10,20] ] )
print(numeros)

matriz_flotantes = np.array( [ [10,20], [30, 40.2] ] )
print(matriz_flotantes)

matriz_string = np.array( [ [10,20], [40.2, '50'] ] )
print(matriz_string)

#Inicializar una matriz con ceros
matriz = np.zeros( [4,5] )
print(matriz)

print('---------------')

#Inicializar una matriz con unos
matriz = np.ones( [100,20] )
print(matriz)

#Crear/retornar matriz con probabilidades

nombre = [ 'Jair', 'Robert', 'Fabián', 'Juan', 'Yeyson', 'Kelly', 'Camilo', 'Fernando', 'Luis' ]

ganadores = r.choice( nombre, size=r.choice([1,2], p=[0.2, 0.8]), p=[0.1,0.1,0.1,0.1,0.1,0.1,0.0,0.0,0.4], replace=False )

print('Ganadores de la rifa Misión TIc: ')
print(ganadores)


numeros_1 = np.array([ [20,10,50], [10,90,60] ])
numeros_2 = np.array([ [90,80,70], [60,50,40] ])

print('----------SUMA---------')
suma = np.add( numeros_1, numeros_2 )
suma = numeros_1 + numeros_2
print(suma)

print('-------RESTA------')
resta = numeros_1 - numeros_2
resta = np.subtract(numeros_1, numeros_2)
print(resta)

print('--------DIVIDIR--------')
dividir = numeros_1 / numeros_2
print(dividir)

print('--------MULTIPLICAR ELEMENTO A ELEMENTO-------')
multiplicacion = numeros_1 * numeros_2
print(multiplicacion)

print('----------MULTIPLICACIÓN DE MATRICES-------')
matriz_1 = np.array([ [20,10,50], [10,90,60] ])
matriz_2 = np.array([ [2, 4], [4,5], [6,8] ])
multiplicacion = np.dot(matriz_1, matriz_2)
print(multiplicacion)