from numpy import random as r


def crear_palos(lista_cartas):
    # Tarea: realizar la mejora solo con map
    trebol = ['Trebol' for n in lista_cartas]
    corazones = ['Corazón' for n in lista_cartas]
    picas = list(map(lambda n: 'Picas', lista_cartas))
    diamantes = ['Diamante' for n in lista_cartas]
    # -------------------
    trebol = list(zip(lista_cartas, trebol))
    corazones = list(zip(lista_cartas, corazones))
    picas = list(zip(lista_cartas, picas))
    diamantes = list(zip(lista_cartas, diamantes))
    cartas = trebol + corazones + picas + diamantes
    # barajar las cartas
    r.shuffle(cartas)

    return cartas


def repartir_cartas(lista_cartas):
    # barajar las cartas
    r.shuffle(lista_cartas)
    # repartir cartas
    carta_1 = lista_cartas.pop()
    carta_2 = lista_cartas.pop()
    return carta_1, carta_2


def poner_en_mesa(lista_cartas):
    carta_1 = lista_cartas.pop()
    carta_2 = lista_cartas.pop()
    carta_3 = lista_cartas.pop()
    return (carta_1, carta_2, carta_3)


cartas = ['A', '1', '2', '3', '4', '5',
          '6', '7', '8', '9', '10', 'J', 'Q', 'K']
lista_cartas = crear_palos(cartas)
mesa = poner_en_mesa(lista_cartas)
print('Cartas sobre la mesa: ', mesa)
print('Cartas disponibles: ', lista_cartas)
mis_cartas = repartir_cartas(lista_cartas)
print('Mis cartas: ', mis_cartas)
